---
# Feel free to add content and custom Front Matter to this file.
# To modify the layout, see https://jekyllrb.com/docs/themes/#overriding-theme-defaults

layout: home
---

# Lista de cursos

- [2018/06 - UASB - Desarrollo de software en entornos libres](https://cursos.rednegra.net/201806-UASB-desarrollo-de-software-en-entornos-libres/)
- [2018/05 - EMI - Seguridad en bases de datos relacionales](https://cursos.rednegra.net/201805-EMI-seguridad-en-bases-de-datos-relacionales/)
