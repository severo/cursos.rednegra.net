---
layout: page
title: Más información
permalink: /about/
---

En esta plataforma, publico los soportes de los cursos que doy. Todos los cursos son publicados bajo licencia [CC-BY](https://creativecommons.org/licenses/by/4.0/). El código de este sitio, hecho con Jekyll, se encuentra en [Framagit](https://framagit.org/cursos-rednegra/cursos.rednegra.net/).

Si estabas buscando información sobre mi, ver [https://rednegra.net/](https://rednegra.net/sylvainlesage/).
